#include<stdio.h>
int
main ()
{
  int n, d, rev = 0, originalnumber;
  printf ("enter the number:");
  scanf ("%d", &n);
  originalnumber = n;
  while (n != 0)
    {
      d = n % 10;
      rev = rev * 10 + d;
      n = n / 10;
    }
  printf ("reverse number=%d\n", rev);
  if (originalnumber == rev)
    {
      printf ("the number is palindrome=%d\n", originalnumber);
    }
  else
    {
      printf ("the number is not palindrome=%d\n", originalnumber);
    }
  return 0;
}
